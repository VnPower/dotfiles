
# What’s unique?
- **Minimal and verbose**: I like minimal UI design, not the kind of
  "minimal" and aesthetic UI design on r/unixporn though, they waste too much
  spaces on unnessesary paddings and margins. I also like to retain as
  much information on my screen as possible.
- **Very little eyecandy**: There are absolutely zero animations or
  flashy elements in my
  setup, except for the wallpaper since I rarely see my wallpaper
  anyway. My [window manager](https://labwc.github.io) contains nothing more than titlebars,
  drop shadows and transparency, which is good for my eyes.
- **Unified looks**: GTK, QT, applications in general should share the same theme.

# Tour

|                                                   |                                           |
|---------------------------------------------------|----------------------------------------------|
| GTK, QT, Electron, native Wayland, and native X11 | **Floorp** and **Emacs**                     |
| ![s](https://mei.kuudere.pw/F4KbpY4LM7K.png)      | ![s](https://mei.kuudere.pw/IRi3Fuh5iFJ.png) |
| Some UI widgets (incomplete!! still working on this) | |
| ![s](https://mei.kuudere.pw/LVSy6gv3BE4.png)         | |

# Installation
I do not recommend you to try to install my configurations, since, at this moment, my configs are not very polished, contains many parts that are not open to the public and it is just not written for anything other than me. My config should only be used as a reference.

If you do want to get this running, don't expect for it to work
out-of-the-box. I will not provide support on this matter.

I do have a [XMPP group
chat](https://xmpp.link/#vnpmuc@muc.step.im%3Fjoin) open if you wanted
to ask me anything about NixOS or other general stuff.

# History

*[extended version](https://loang.net/~vnpower/posts/short-nixos/) on my website*

Despite having spent years daily driving on Linux, I never had a decent dotfiles
repository/desktop config before. That changed at the beginning of this year however,
since I finally got the courage to try out [NixOS](https://nixos.org) - the Linux
distribution that is [notorious](https://ersei.net/en/blog/its-nixin-time) for its steep learning curve - after
reading [d-rens’](https://d-rens.xyz/blog/little-efforts-that-make-life-easier/) blog post on things that repaid him well for the
efforts he put into them. And honestly, I do think that learning NixOS
has made the process of customizing my desktop more fun and...
addicting.

It felt like I found out what has been preventing me from having a decent
desktop configuration. It just that I have never felt so hooked into
writing config files before.

The Linux experience just hasn’t been the same since the day I switched to NixOS, and though the
process of configuring did become a bit more grueling and there were also
 some things that doesn’t work as I expected, I do think that
it is worth the learning effort to get what NixOS has to offer.

# License
[MIT](LICENSE)

# Credits
- https://git.sr.ht/~rycee/configurations
- https://codeberg.org/kye/nixos
- https://git.sr.ht/~d-rens/nixos-dotfiles
- https://gitlab.com/folliehiyuki/nixconfig
- https://github.com/hlissner/dotfiles

*Special thanks to [d-rens](https://d-rens.xyz), [Nguyễn Huy
Hoàng](https://www.folliehiyuki.com/), [cnx](https://cnx.gdn)*
