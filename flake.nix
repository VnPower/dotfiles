{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix.url = "github:Mic92/sops-nix";

    home-manager = {
	    url = "github:nix-community/home-manager";
	    inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-alien.url = "github:thiagokokada/nix-alien";

    oisd = {
      url = "https://big.oisd.nl/domainswild";
      flake = false;
    };
  };

  outputs = inputs@{ self, nixpkgs, nixpkgs-stable, nur, sops-nix, home-manager, nix-alien, oisd }:
    let
      system = "x86_64-linux";
	    lib = nixpkgs.lib;
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      nixosConfigurations = {
	      vnpower = lib.nixosSystem {
		      inherit system;
          specialArgs = {inherit inputs;};
		      modules = [
			      ./nixos

            nur.modules.nixos.default
            sops-nix.nixosModules.sops
			      home-manager.nixosModules.home-manager {
				      home-manager = {
                useGlobalPkgs = true;
				        useUserPackages = true;
                backupFileExtension = "old";
                extraSpecialArgs = {
                  inherit inputs;

                  pkgs-stable = import nixpkgs-stable {
                    inherit system;
                  };
                };

                users.vnpower.imports = [
                  inputs.nur.modules.homeManager.default
                  sops-nix.homeManagerModules.sops
                 ./home
                ];
              };
			      }
		      ];
	      };
      };
    };
}
