_: {
  programs.fuzzel = {
		enable = true;

    settings = {
      main = {
        font = "IBM Plex Mono";
        dpi-aware = "auto";
        fields = "filename,name,generic";
        icons-enabled = "yes";
        match-mode = "fzf";
        show-actions = "no";
        layer = "top";
        exit-on-keyboard-focus-loss = "yes";
        anchor = "bottom";
        y-margin=10;
        lines=10;
        width=30;
        tabs=4;
        horizontal-pad = 18;
        inner-pad = 0;
        image-size-ratio = 0.5;
        line-height = 20;
      };

      border = {
        width = 0;
        radius = 8;
      };

      colors = {
        background = "1f1f28ff";
        text = "dcd7baff";
        match = "cb4b16ff";
        selection = "2d4f67ff";
        selection-text = "c8c093ff";
        selection-match = "cb4b16ff";
        border = "7E9CD8ff";
      };
    };
	};
}
