# TODO: configure my emails with accounts.email options
_: {
  programs.thunderbird = {
    enable = false;
    # Ref: https://github.com/HorlogeSkynet/thunderbird-user.js/blob/master/user.js
    settings = {
      "browser.newtabpage.enabled" = false;
      "mailnews.start_page.enabled" = false;
      "mailnews.start_page.url" = "";

      "geo.provider.ms-windows-location" = false;
      "geo.provider.use_corelocation" = false;
      "geo.provider.use_geoclue" = false;

      "extensions.getAddons.showPane" = false;
      "extensions.htmlaboutaddons.recommendations.enabled" = false;
      "browser.shopping.experience2023.enabled" = false;

      "datareporting.policy.dataSubmissionEnabled" = false;
      "datareporting.policy.dataSubmissionPolicyBypassNotification" = true;
      "datareporting.healthreport.uploadEnabled" = false;
      "toolkit.telemetry.unified" = false;
      "toolkit.telemetry.enabled" = false;
      "toolkit.telemetry.server" = "data:,";
      "toolkit.telemetry.archive.enabled" = false;
      "toolkit.telemetry.newProfilePing.enabled" = false;
      "toolkit.telemetry.shutdownPingSender.enabled" = false;
      "toolkit.telemetry.updatePing.enabled" = false;
      "toolkit.telemetry.bhrPing.enabled" = false;
      "toolkit.telemetry.firstShutdownPing.enabled" = false;
      "toolkit.telemetry.coverage.opt-out" = true;
      "toolkit.coverage.opt-out" = true;
      "toolkit.coverage.endpoint.base" = "";

      "app.donation.eoy.version.viewed" = 999;
      "app.shield.optoutstudies.enabled" = false;
      "app.normandy.enabled" = false;
      "app.normandy.api_url" = "";

      "breakpad.reportURL" = "";
      "browser.tabs.crashReporting.sendReport" = false;
      "browser.crashReports.unsubmittedCheck.autoSubmit2" = false;

      "captivedetect.canonicalURL" = "";
      "network.captive-portal-service.enabled" = false;
      "network.connectivity-service.enabled" = false;
      "mail.instrumentation.postUrl" = "";
      "mail.instrumentation.askUser" = false;
      "mail.instrumentation.userOptedIn" = false;
      "mail.rights.override" = true;

      "browser.safebrowsing.blockedURIs.enabled" = false;
      "browser.safebrowsing.downloads.enabled" = false;
      "browser.safebrowsing.malware.enabled" = false;
      "browser.safebrowsing.phishing.enabled" = false;
      "browser.safebrowsing.downloads.remote.enabled" = false;

      "network.prefetch-next" = false;
      "network.dns.disablePrefetch" = true;
      "network.dns.disablePrefetchFromHTTPS" = true;
      "network.predictor.enabled" = false;
      "network.predictor.enable-prefetch" = false;
      "network.http.speculative-parallel-limit" = 0;
      "browser.meta_refresh_when_inactive.disabled" = true;

      "network.proxy.socks_remote_dns" = true;
      "network.file.disable_unc_paths" = true;
      "network.gio.supported-protocols" = "";

      "browser.urlbar.speculativeConnect.enabled" = false;
      "browser.urlbar.quicksuggest.enabled" = false;
      "browser.urlbar.suggest.quicksuggest.nonsponsored" = false;
      "browser.urlbar.suggest.quicksuggest.sponsored" = false;
      "browser.search.suggest.enabled" = false;
      "browser.urlbar.suggest.searches" = false;
      "browser.urlbar.trending.featureGate" = false;
      "browser.urlbar.addons.featureGate" = false;
      "browser.urlbar.mdn.featureGate" = false;
      "browser.urlbar.weather.featureGate" = false;
      "browser.urlbar.yelp.featureGate" = false;
      "browser.urlbar.clipboard.featureGate" = false;
      "browser.formfill.enable" = false;
      "browser.urlbar.suggest.engines" = false;
      "layout.css.visited_links_enabled" = false;
      "browser.search.separatePrivateDefault" = true;
      "browser.search.separatePrivateDefault.ui.enabled" = true;

      "signon.autofillForms" = false;
      "signon.formlessCapture.enabled" = false;
      "network.auth.subresource-http-auth-allow" = 1;

      "browser.sessionstore.privacy_level" = 2;
      "toolkit.winRegisterApplicationRestart" = false;
      "browser.shell.shortcutFavicons" = false;
      "mail.imap.use_disk_cache2" = false;

      "security.ssl.require_safe_negotiation" = true;
      "security.tls.enable_0rtt_data" = true;

      "security.OCSP.enabled" = 1;
      "security.OCSP.require" = true;

      "security.cert_pinning.enforcement_level" = 2;
      "security.remote_settings.crlite_filters.enabled" = true;
      "security.pki.crlite_mode" = 2;

      "security.mixed_content.block_display_content" = true;
      "dom.security.https_only_mode" = true;
      "dom.security.https_only_mode_pbm" = true;
      "dom.security.https_only_mode_ever_enabled" = true;
      "dom.security.https_only_mode_ever_enabled_pbm" = true;
      "dom.security.https_only_mode_send_http_background_request" = false;

      "security.ssl.treat_unsafe_negotiation_as_broken" = true;
      "browser.xul.error_pages.expert_bad_cert" = true;
      "security.warn_entering_weak" = true;
      "security.warn_leaving_secure" = true;
      "security.warn_viewing_mixed" = true;

      "network.http.referer.XOriginTrimmingPolicy" = 2;
      "privacy.userContext.enabled" = true;
      "privacy.userContext.ui.enabled" = true;

      "media.peerconnection.ice.proxy_only_if_behind_proxy" = true;
      "media.peerconnection.ice.default_address_only" = true;
      "media.peerconnection.ice.no_host" = true;
      "media.gmp-provider.enabled" = false;

      "dom.disable_window_move_resize" = true;

      "browser.uitour.enabled" = false;
      "browser.uitour.url" = "";
      "devtools.debugger.remote-enabled" = false;
      "permissions.manager.defaultsUrl" = "";
      "webchannel.allowObject.urlWhitelist" = "";
      "network.IDN_show_punycode" = true;
      "pdfjs.enableScripting" = false;
      "browser.tabs.searchclipboardfor.middleclick" = false;
      "browser.contentanalysis.enabled" = false;
      "browser.contentanalysis.default_result" = 0;

      "browser.download.useDownloadDir" = false;
      "browser.download.manager.addToRecentDocs" = false;
      "browser.download.always_ask_before_handling_new_types" = true;

      "privacy.sanitize.sanitizeOnShutdown" = true;
      "privacy.sanitize.timeSpan" = 0;
      "privacy.clearOnShutdown.cache" = true;
      "privacy.clearOnShutdown_v2.cache" = true;
      "privacy.clearOnShutdown.downloads" = true;
      "privacy.clearOnShutdown.formdata" = true;
      "privacy.clearOnShutdown.history" = true;
      "privacy.clearOnShutdown_v2.historyFormDataAndDownloads" = true;
      "privacy.clearOnShutdown.siteSettings" = true;
      "privacy.clearOnShutdown_v2.siteSettings" = true;
      "privacy.clearOnShutdown.cookies" = true;
      "privacy.clearOnShutdown.offlineApps" = true;
      "privacy.clearOnShutdown.sessions" = true;
      "privacy.clearOnShutdown_v2.cookiesAndStorage" = true;
      "privacy.clearSiteData.cache" = true;
      "privacy.clearSiteData.cookiesAndStorage" = true;
      "privacy.clearSiteData.historyFormDataAndDownloads" = true;
      "privacy.clearSiteData.siteSettings" = true;
      "privacy.cpd.cache" = true;
      "privacy.clearHistory.cache" = true;
      "privacy.cpd.formdata" = true;
      "privacy.cpd.history" = true;
      "privacy.clearHistory.historyFormDataAndDownloads" = true;
      "privacy.cpd.cookies" = true;
      "privacy.cpd.sessions" = true;
      "privacy.cpd.offlineApps" = true;
      "privacy.clearHistory.cookiesAndStorage" = true;
      "privacy.cpd.openWindows" = true;
      "privacy.cpd.passwords" = true;
      "privacy.cpd.siteSettings" = true;
      "privacy.clearHistory.siteSettings" = true;

      "privacy.resistFingerprinting" = true;
      "privacy.resistFingerprinting.pbmode" = true;
      "privacy.resistFingerprinting.block_mozAddonManager" = true;
      "privacy.resistFingerprinting.letterboxing" = true;
      "privacy.window.maxInnerWidth" = 1600;
      "privacy.window.maxInnerHeight" = 900;

      "privacy.spoof_english" = 1;
      "browser.display.use_system_colors" = false;
      "browser.link.open_newwindow" = 3;
      "browser.link.open_newwindow.restriction" = 0;
      "webgl.disabled" = true;

      "places.history.enabled" = false;
      "extensions.formautofill.addresses.enabled" = false;
      "extensions.formautofill.creditCards.enabled" = false;
      "dom.popup_allowed_events" = "click dblclick mousedown pointerdown";
      "browser.pagethumbnails.capturing_disabled" = true;

      "mathml.disabled" = true;
      "svg.disabled" = true;
      "gfx.font_rendering.graphite.enabled" = false;
      "javascript.enabled" = false;
      "javascript.options.asmjs" = false;
      "javascript.options.ion" = false;
      "javascript.options.baselinejit" = false;
      "javascript.options.jit_trustedprincipals" = true;
      "javascript.options.wasm" = false;
      "gfx.font_rendering.opentype_svg.enabled" = false;
      "media.eme.enabled" = false;
      "browser.eme.ui.enabled" = false;
      "network.http.referer.XOriginPolicy" = 2;
      "security.external_protocol_requires_permission" = true;
      "geo.enabled" = false;
      "full-screen-api.enabled" = false;
      "network.http.sendRefererHeader" = 0;
      "network.http.referer.trimmingPolicy" = 0;
      "network.http.referer.defaultPolicy" = 0;
      "network.http.referer.defaultPolicy.pbmode" = 0;
      "network.http.altsvc.enabled" = false;
      "dom.event.contextmenu.enabled" = false;
      "media.mediasource.enabled" = false;
      "media.hardware-video-decoding.enabled" = false;
      "media.peerconnection.enabled" = false;
      "permissions.default.image" = 2;

      "browser.startup.homepage_override.mstone" = "ignore";
      "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons" = false;
      "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features" = false;

      "mailnews.auto_config.guess.enabled" = false;
      "mailnews.auto_config.guess.sslOnly" = true;
      "mailnews.auto_config.guess.requireGoodCert" = true;
      "mailnews.auto_config.fetchFromISP.enabled" = false;
      "mailnews.auto_config.fetchFromISP.sendEmailAddress" = false;
      "mailnews.auto_config.fetchFromISP.sslOnly" = true;
      "mail.provider.enabled" = false;

      "mail.cloud_files.enabled" = false;
      "pref.privacy.disable_button.view_cookies" = false;
      "pref.privacy.disable_button.cookie_exceptions" = false;
      "pref.privacy.disable_button.view_passwords" = false;

      "mailnews.headers.showSender" = true;
      "mailnews.headers.showUserAgent" = true;
      "mail.smtpserver.default.hello_argument" = "[127.0.0.1]";
      "mailnews.display.original_date" = false;
      "mailnews.display.date_senders_timezone" = false;
      "mailnews.headers.sendUserAgent" = false;

      "mailnews.reply_header_type" = 1;
      "mailnews.reply_header_authorwrotesingle" = "#1 wrote:";
      "mail.suppress_content_language" = true;
      "mail.sanitize_date_header" = true;

      "spellchecker.dictionary" = "en-US";
      "mail.identity.default.compose_html" = false;
      "mail.default_send_format" = 1;
      "mailnews.display.disallow_mime_handlers" = 3;
      "mailnews.display.html_as" = 3;
      "mail.html_sanitize.drop_conditional_css" = true;
      "mailnews.display.prefer_plaintext" = false;
      "mail.inline_attachments" = false;
      "mail.compose.big_attachments.notify" = true;
      "mail.compose.add_link_preview" = false;

      "mail.phishing.detection.enabled" = true;
      "mail.phishing.detection.disallow_form_actions" = true;
      "mail.phishing.detection.ipaddresses" = true;
      "mail.phishing.detection.mismatched_hosts" = true;
      "mailnews.message_display.disable_remote_image" = true;

      "mail.chat.enabled" = false;
      "purple.logging.log_chats" = false;
      "purple.logging.log_ims" = false;
      "purple.logging.log_system" = false;
      "purple.conversations.im.send_typing" = false;

      "calendar.timezone.local" = "UTC";
      "calendar.timezone.useSystemTimezone" = false;
      "calendar.extract.service.enabled" = false;

      "rss.show.content-base" = 3;
      "rss.show.summary" = 1;
      "rss.message.loadWebPageOnSelect" = 0;

      # Ref: https://codeberg.org/12bytes/thunderbird-user.js-supplement/src/branch/master/user-overrides.js
      "app.update.auto" = false;
      "offline.autoDetect" = false;
      "toolkit.legacyUserProfileCustomizations.stylesheets" = false;
      "view_source.syntax_highlight" = true;
      "mail.mdn.report.enabled" = false;
      "browser.search.update" = false;
      "general.useragent.compatMode.firefox" = true;

      # My own preferences
      # - Always show window title bar
      "mail.tabs.drawInTitlebar" = false;
      # - Don't save cookies
      "network.cookie.cookieBehavior" = 2;
      "network.cookie.cookieBehavior.pbmode" = 2;
      "network.cookie.cookieBehavior.optInPartitioning" = true;
      "network.cookie.cookieBehavior.optInPartitioning.pbmode" = true;
      # - Don't ask when I go online
      "offline.download.download_messages" = 1;
      # - Enable adaptive junk filter logging
      "mail.spam.logging.enabled" = true;
    };

    # At least 1 profile needs to be configured
    profiles.default = {
      isDefault = true;
      withExternalGnupg = true;
      search = {
        default = "SearX-Auto";
        privateDefault = "SearX-Auto";
        engines = import ../floorp/search-engines.nix;
        force = true;
      };
    };
  };
}
