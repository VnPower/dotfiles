{ pkgs, ... }:
{
  services.emacs = {
    enable = true;
    client.enable = true;
    startWithUserSession = "graphical";
  };

  programs.emacs = {
	  enable = true;
	  package = pkgs.emacs30-pgtk;
    extraPackages =
      epkgs: with epkgs; [
        (treesit-grammars.with-grammars (
          p: with p; [
            tree-sitter-bash
            tree-sitter-c
            tree-sitter-cmake
            tree-sitter-css
            tree-sitter-scss
            tree-sitter-dockerfile
            tree-sitter-elisp
            tree-sitter-fish
            tree-sitter-go
            tree-sitter-html
            tree-sitter-javascript
            tree-sitter-json
            tree-sitter-make
            tree-sitter-markdown
            tree-sitter-markdown-inline
            tree-sitter-nix
            tree-sitter-python
            tree-sitter-ruby
            tree-sitter-rust
            tree-sitter-tsx
            tree-sitter-toml
            tree-sitter-typescript
            tree-sitter-yaml
            tree-sitter-zig
          ]
        ))
    ];
  };

  xdg.configFile = {
    "emacs/init.el".source = ./init.el;
    "emacs/early-init.el".source = ./early-init.el;
  };
}
