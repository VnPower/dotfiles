(with-eval-after-load 'scroll-bar
  (scroll-bar-mode -1))

(with-eval-after-load 'tool-bar
  (tool-bar-mode -1))

(with-eval-after-load 'menu-bar
  (menu-bar-mode -1))

(with-eval-after-load 'comp
  (setopt native-comp-async-jobs-number 8
          native-comp-speed 1
          native-comp-always-compile t))

;; Android stuff
(when (string-equal system-type "android")
  ;; Add Termux binaries to PATH environment
  (let ((termuxpath "/data/data/com.termux/files/usr/bin"))
    (setenv "PATH" (concat (getenv "PATH") ":" termuxpath))
    (setq exec-path (append exec-path (list termuxpath)))))

;; Fix SSL connection for F-Droid version of Emacs.
;;
;; Android port is builded without gnutls support and uses 'gnutls-cli' command.
;; 'gnutls' command line must be installed in Termux.
;; This removes '%t' (filepath with trused certificates) from original command line, as it causes the error.
;; "--x509cafile /data/data/com.termux/files/usr/etc/tls/cert.pem' was also not working".
;; 'openssl' is not working at all.
(when (string-equal system-type "android")
  (setq tls-program '("gnutls-cli -p %p %h"
          "gnutls-cli -p %p %h --protocols ssl3"
          ;"openssl s_client -connect %h:%p -no_ssl2 -ign_eof"
  )))

(with-eval-after-load 'warnings
  ;; native comp の warning を抑える
  (setopt warning-suppress-types '((comp))))
