;; go-translate
;; lin
;; yasnippet
;; hl-todo

;;;; Package repo setup
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu-elpa-devel" . "https://elpa.gnu.org/devel/"))

;; Native compile packages for better performance?
(setq package-native-compile t)

;;;; Functions
;; Shift block of text
(defun shift-text (distance)
  (if (use-region-p)
      (let ((mark (mark)))
        (save-excursion
          (indent-rigidly (region-beginning)
                          (region-end)
                          distance)
          (push-mark mark t t)
          (setq deactivate-mark nil)))
    (indent-rigidly (line-beginning-position)
                    (line-end-position)
                    distance)))

(defun shift-right (count)
  (interactive "p")
  (shift-text count))

(defun shift-left (count)
  (interactive "p")
  (shift-text (- count)))

;;;; Modal editing package
;; Note: I use Colemak so you may have to reconfigure this
(defun meow-setup ()
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-colemak)

  ;; Appending (a) without a selection in meow = Inserting before the cursor
  ;; Setting this variable fixes that
  (setq meow-use-cursor-position-hack t)
  (meow-motion-overwrite-define-key
   ;; Use e to move up, n to move down.
   ;; Since special modes usually use n to move down, we only overwrite e here.
   '("e" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   '("?" . meow-cheatsheet)
   ;; To execute the originally e in MOTION state, use SPC e.
   '("e" . "H-e")
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("f" . find-file)
   '("d" . consult-buffer)
   '("e" . eglot)
   '("g" . magit)
   '("s" . dired-sidebar-toggle-sidebar)
   '("c" . comment-or-uncomment-region)
   '("l" . window-sizable-p)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("1" . meow-expand-1)
   '("2" . meow-expand-2)
   '("3" . meow-expand-3)
   '("4" . meow-expand-4)
   '("5" . meow-expand-5)
   '("6" . meow-expand-6)
   '("7" . meow-expand-7)
   '("8" . meow-expand-8)
   '("9" . meow-expand-9)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . meow-beginning-of-thing)
   '("]" . meow-end-of-thing)
   '("/" . meow-visit)
   '("a" . meow-append)
   '("A" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("e" . meow-prev)
   '("E" . meow-prev-expand)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-right)
   '("I" . meow-right-expand)
   '("j" . meow-join)
   '("k" . meow-kill)
   '("l" . meow-line)
   '("L" . meow-goto-line)
   '("m" . meow-mark-word)
   '("M" . meow-mark-symbol)
   '("n" . meow-next)
   '("N" . meow-next-expand)
   '("O" . execute-extended-command)
   '("o" . meow-to-block)
   '("p" . meow-yank)
   '("q" . meow-quit)
   '("r" . meow-replace)
   '("s" . meow-insert)
   '("S" . meow-open-above)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-search)
   '("w" . meow-next-word)
   '("W" . meow-next-symbol)
   '("d" . meow-delete)
   '("x" . meow-line)
   '("X" . meow-backward-delete)
   '("y" . meow-save)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '(":" . execute-extended-command)
   '("<escape>" . ignore)))

;; Function to reload config file on the spot
(defun reload-conf ()
  (interactive)
  (load-file "~/.emacs.d/init.el"))

;; Garbage collection, TAB key behavior...
(use-package emacs
  :ensure nil
  :demand t
  :config
  (setq gc-cons-threshold (* 10 gc-cons-threshold))
  (setq garbage-collection-messages nil)
  (setq tab-always-indent 'complete)
  (setq tab-first-completion 'word-or-paren-or-punct) ; Emacs 27
  (setq frame-inhibit-implied-resize t)
  (setq-default tab-width 4
                indent-tabs-mode nil))

;;;; Disable "electric" behaviour
(use-package electric
  :ensure nil
  :hook
  (prog-mode . electric-indent-local-mode)
  :config
  ;; I don't like auto indents in Org and related.  They are okay for
  ;; programming.
  (electric-pair-mode 1)
  (electric-quote-mode -1)
  (electric-indent-mode -1))

;; Fancy icons
(use-package nerd-icons
  :ensure t)

(use-package nerd-icons-corfu
  :ensure t
  :after corfu
  :config (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package nerd-icons-completion
  :ensure t
  :hook (after-init . nerd-icons-completion-mode))

;; Org mode - What we are here for.
(use-package org
  :ensure t
  :config
  (setq org-src-fontify-natively t
        org-src-window-setup 'current-window
        org-src-strip-leading-and-trailing-blank-lines t
        org-src-preserve-indentation t
        org-imenu-depth 3
        org-src-tab-acts-natively nil
        org-enable-priority-commands t
        org-highest-priority ?A
        org-default-priority ?E
        org-lowest-priority ?E
        org-todo-keywords '((sequence "TODO" "WORKING" "DONE" "POSTPONED"))
        ))

(add-to-list 'org-structure-template-alist '("," . "src emacs-lisp"))

(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))

(use-package org-indent
  :ensure nil
  :hook (org-mode . org-indent-mode))

;;;; Modes
;;;; Plain text (text-mode)
(use-package text-mode
  :ensure nil
  :mode "\\`\\(README\\|CHANGELOG\\|COPYING\\|LICENSE\\)\\'"
  :hook
  ((text-mode . turn-on-auto-fill)
   (prog-mode . (lambda () (setq-local sentence-end-double-space t))))
  :config
  (setq sentence-end-double-space nil)
  (setq sentence-end-without-period nil)
  (setq colon-double-space nil)
  (setq use-hard-newlines nil)
  (setq adaptive-fill-mode t))

(use-package yaml-mode :ensure t)

(use-package go-mode
  :ensure t
  :hook (go-mode . eglot-ensure))

(use-package lua-mode :ensure t)
(use-package fennel-mode :ensure t)
(use-package ruby-mode :ensure t)
(use-package markdown-mode :ensure t)
(use-package zig-mode :ensure t)
(use-package clojure-mode
  :ensure t
  :config
  (setq clojure-indent-style 'always-indent
        clojure-indent-keyword-style 'always-indent
        clojure-enable-indent-specs nil))

(use-package svelte-mode :ensure t)
(use-package web-mode :ensure t)
(use-package css-mode :ensure t)
(use-package emmet-mode :ensure t)
(autoload 'emmet-expand-line "emmet-mode" nil t)

(add-hook 'sgml-mode-hook 'emmet-mode)
(add-hook 'web-mode-hook 'emmet-mode)

(use-package nix-mode
  :ensure t
  :mode "\\.nix\\'")

(use-package meow
  :ensure t
  :config
  (setq meow-use-clipboard t))

(meow-setup)
(meow-setup-indicator)
(meow-global-mode 1)

(meow-define-keys 'normal
  '("<" . (lambda () (interactive)
	        (shift-left 2))))

(meow-define-keys 'normal
  '(">" . (lambda () (interactive)
	        (shift-right 2))))

;; Minibuffer
(use-package vertico
  :ensure t
  :config
  (setq vertico-resize nil
	    vertico-count 8
	    vertico-cycle t))

(vertico-mode)

(use-package consult
  :ensure t)

(use-package consult-dir
  :after consult
  :ensure t)

(use-package consult-notes
  :ensure t
  :config
  (setq consult-notes-file-dir-sources '(("Denote" ?d "~/Documents/notes"))))

;; Optionally configure the register formatting. This improves the register
;; preview for `consult-register', `consult-register-load',
;; `consult-register-store' and the Emacs built-ins.
(setq register-preview-delay 0.5
      register-preview-function #'consult-register-format)

;; Optionally tweak the register preview window.
;; This adds thin lines, sorting and hides the mode line of the window.
(advice-add #'register-preview :override #'consult-register-window)

(use-package marginalia
  :ensure t
  :init
  (marginalia-mode +1))


(use-package orderless
  :ensure t
  :config
  (with-eval-after-load 'minibuffer
    (setopt completion-styles '(orderless basic)
            completion-category-overrides '((file (styles basic partial-completion))))))

;; Font

(setq-default line-spacing 0.1)

(use-package fontaine
  :ensure t
  :config
  (setq fontaine-presets
        '((regular
           :default-family "PlemolJP35 Console NF"
           :fixed-pitch-family "PlemolJP35 Console NF"
           :variable-pitch-family "IBM Plex Sans"
           :variable-pitch-weight normal
           :variable-pitch-height 1
           :line-spacing 1
           :italic-family "PlemolJP35 Console NF"
           :default-height 101
           )
          (large
           :default-family "PlemolJP35 Console NF"
           :variable-pitch-family "IBM Plex Sans"))))

(fontaine-set-preset (or (fontaine-restore-latest-preset) 'regular))
(add-hook 'kill-emacs-hook #'fontaine-store-latest-preset)

(use-package variable-pitch-mode
  :ensure nil
  :hook
  (org-mode . variable-pitch-mode))

(use-package visual-line-mode
  :ensure nil
  :hook
  (org-mode . visual-line-mode))

(use-package visual-fill-column
  :ensure t
  :hook (org-mode . visual-fill-column-mode)
  :custom
  (visual-fill-column-center-text t)
  (visual-fill-column-width 110))

;; Themes
(use-package ef-themes
  :ensure t
  :config
  (setq ef-themes-mixed-fonts t
        ef-themes-variable-pitch-ui t))

(use-package doom-themes
  :ensure t)

;; Load themes and disable the frame background
(use-package kanagawa-themes
  :ensure t)

;; Transparent background
(if (display-graphic-p)
    (progn
      (add-to-list 'default-frame-alist '(alpha-background . 98))
      (set-frame-parameter nil 'alpha-background 98))
  (set-face-background 'default "undefined"))

(load-theme 'kanagawa-wave t)

(use-package rainbow-mode
  :ensure t
  :init (add-hook 'prog-mode-hook 'rainbow-mode))

;; Rainbow brackets
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

;; Modeline
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(use-package vundo
  :ensure t)

;; Pop-up completions
(use-package corfu
  :ensure t
  :hook ((prog-mode . corfu-mode))
  :bind
  ;; Tab-complete
  (:map corfu-map
        ("TAB" . corfu-next)
        ("<tab>" . corfu-next)
        ("S-TAB" . corfu-previous)
        ("<backtab>" . corfu-previous))
  :config
  (setq corfu-auto t
        corfu-auto-delay 0.05
        corfu-popupinfo-delay 0.3
        corfu-auto-prefix 2
        corfu-cycle t
        corfu-preselect 'prompt
        corfu-quit-no-match 'separator
        text-mode-ispell-word-completion nil)

  ;; Exit on ESC
  (with-eval-after-load 'meow
    (define-key corfu-map (kbd "<escape>")
                (lambda ()
                  (interactive)
                  (corfu-quit)
                  (meow-normal-mode))))
  (global-corfu-mode)
  (corfu-popupinfo-mode))

;; LSP
(use-package eglot
  :ensure t
  :after corfu
  :hook (prog-mode . eglot-ensure)
  :bind ( :map eglot-mode-map
          ("C-c r" . eglot-rename)
          ("C-c o" . eglot-code-action-organize-imports)
          ("C-c a" . eglot-code-actions)
          ("C-c h" . eldoc)
          ("<f6>" . xref-find-definitions))
  :config
  (setq eglot-events-buffer-size 0)
  (setq eglot-autoshutdown t)
  (setq eglot-stay-out-of '(yasnippet))
  (setq eglot-sync-connect nil)

  ;; Turn off event logs
  (with-eval-after-load "eglot"
    (setq eglot-events-buffer-config '(:size 0 :format full))
    (setq eglot-events-buffer-size 0))

  (with-eval-after-load "jsonrpc"
    (fset #'jsonrpc--log-event #'ignore)
    (setq jsonrpc-event-hook nil)))

;; LaTeX
(use-package auctex :ensure t)
(use-package cdlatex :ensure t)

(setq org-format-latex-options
      (quote (:foreground default :scale 1.8)))

;; Typst


;; Documentation access
(use-package eldoc
  :ensure t
  :hook (prog-mode . eldoc-mode)
  :config
  (setq eldoc-idle-delay 0.5))

(global-eldoc-mode)

;; Always indent mode
(use-package aggressive-indent
  :ensure t
  :hook ((lisp-mode emacs-lisp-mode) . aggressive-indent-mode))

;; Add paddings to some UI components
(use-package spacious-padding
  :ensure t
  :config
  (setq spacious-padding-widths
        '(:header-line-width 4
                             :mode-line-width 0
                             :tab-width 4
                             :right-divider-width 0
                             :scroll-bar-width 8
                             :left-fringe-width 4
                             :right-fringe-width 8)))

(spacious-padding-mode 1)


;; Set margin for org and md
(defun center-window (window) ""
       (let* ((current-extension (file-name-extension (or (buffer-file-name) "foo.unknown")))
              (max-text-width 80)
              (margin (max 0 (/ (- (window-width window) max-text-width) 2))))
         (if (and (not (string= current-extension "md"))
                  (not (string= current-extension "org")))
             ;; Do nothing if this isn't an .md or .txt file.
             ()
           (set-window-margins window margin margin))))

;; Adjust margins of all windows.
(defun center-windows () ""
       (walk-windows (lambda (window) (center-window window)) nil 1))

;; Listen to window changes.
;; (add-hook 'window-configuration-change-hook 'center-windows)

(use-package perfect-margin
  :ensure t)
(add-hook 'markdown-mode-hook #'perfect-margin-mode)
(add-hook 'org-mode-hook #'perfect-margin-mode)

;; Show changes on version controlled files
(use-package diff-hl
  :ensure t
  :config
  (global-diff-hl-mode))

(use-package which-key
  :ensure t
  :config
  (which-key-mode +1))

;; Magit
(use-package magit
  :ensure t)

;; Denote
(use-package denote
  :ensure t
  :init
  (with-eval-after-load 'org
    (setq denote-directory "~/Documents/notes/"))

  :config
  (with-eval-after-load 'meow
    (meow-leader-define-key
     '("d" . denote-open-or-create)))

  (denote-rename-buffer-mode +1))

;;
(use-package puni
  :ensure t
  :config
  (puni-global-mode +1))

;; Save recent edits
(use-package recentf
  :ensure t
  :init
  (setq recentf-max-saved-items 100)
  (recentf-mode +1))

(use-package flyspell
  :ensure t
  :config
  (setq flyspell-issue-message-flag nil)
  (setq flyspell-issue-welcome-flag nil)
  (setq ispell-program-name "aspell")
  (setq ispell-dictionary "en_US"))

;; Encoding
(set-language-environment "Japanese")
(set-default-coding-systems 'utf-8-unix)
(set-terminal-coding-system 'utf-8-unix)
(setq default-file-name-coding-system 'utf-8)
(setq default-process-coding-system '(utf-8 . utf-8))
(prefer-coding-system 'utf-8-unix)

;; Confirm before exiting
(setq confirm-kill-emacs 'y-or-n-p)

;; Absolutely don't create any kind of backup files
(setq create-lockfiles nil
      make-backup-files nil
      auto-save-default nil
      backup-inhibited t
      delete-auto-save-files t
      backup-directory-alist '(("." . "~/.emacs.d/backup")))

;; Disable default dashboard
(setq inhibit-startup-buffer-menu t
      inhibit-startup-screen t)

;; Let the compiler works in silence
(setq native-comp-async-report-warnings-errors 'silent)

(auto-save-visited-mode 1)

(show-paren-mode t)

(savehist-mode 1)
(save-place-mode t)

;; Revert if any external change
(global-auto-revert-mode +1)

;; Show line numbers
(display-line-numbers-mode)

;; Display the column guide.
(display-fill-column-indicator-mode)

(setq-default show-trailing-whitespace t)

(setq idle-update-delay 1.0)

(setq fast-but-imprecise-scrolling t)

;; Delete trailing whitespaces before saving
(add-hook 'before-save-hook #'delete-trailing-whitespace)

;; Function aliases
(defalias 'w 'save-buffer)
(defalias 'wq 'save-buffer)
(defalias 'q 'save-buffers-kill-terminal)
(defalias 'd 'dired)
(defalias 'dw 'delete-window)
(defalias 'g 'magit)
(defalias 'e 'eglot)
(defalias 'dd 'dired-sidebar-toggle-sidebar)
(defalias 'cr 'comment-or-uncomment-region)

;; Start server/daemon if not running
(require 'server)
(unless (server-running-p)
  (server-start))

;; Credits:
;; https://apribase.net/
;; https://codeberg.org/kotatsuyaki/.emacs.d
;; https://github.com/syohex/dot_files
;; https://github.com/rexim/dotfiles
;; https://github.com/andreyorst/dotfiles
;; https://qiita.com/nobuyuki86/items/122e85b470b361ded0b4

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("cee5c56dc8b95b345bfe1c88d82d48f89e0f23008b0c2154ef452b2ce348da37" "1ad12cda71588cc82e74f1cabeed99705c6a60d23ee1bb355c293ba9c000d4ac" "90185f1d8362727f2aeac7a3d67d3aec789f55c10bb47dada4eefb2e14aa5d01" default))
 '(package-selected-packages
   '(emacs-lisp-compilation-parse-errors-filename-function magit treesit-auto fontaine marginalia org-modern web-mode vundo vertico svelte-mode spacious-padding rainbow-mode org-bullets orderless nim-mode nerd-icons-corfu nano-modeline meow markdown-mode kanagawa-themes go-mode emmet-mode ef-themes doom-themes doom-modeline dimmer diff-hl corfu consult-dir base16-theme aggressive-indent)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fringe ((t :background "#1F1F28")))
 '(header-line ((t :box (:line-width 4 :color "#2A2A37" :style nil))))
 '(header-line-highlight ((t :box (:color "#DCD7BA"))))
 '(keycast-key ((t)))
 '(line-number ((t :background "#1F1F28")))
 '(mode-line ((t)))
 '(mode-line-active ((t)))
 '(mode-line-highlight ((t :box (:color "#DCD7BA"))))
 '(mode-line-inactive ((t)))
 '(tab-bar-tab ((t :box (:line-width 4 :color "grey85" :style nil))))
 '(tab-bar-tab-inactive ((t :box (:line-width 4 :color "grey75" :style nil))))
 '(tab-line-tab ((t)))
 '(tab-line-tab-active ((t)))
 '(tab-line-tab-inactive ((t)))
 '(vertical-border ((t :background "#1F1F28" :foreground "#1F1F28")))
 '(window-divider ((t nil)))
 '(window-divider-first-pixel ((t nil)))
 '(window-divider-last-pixel ((t nil))))
