{ config, ... }:

let
  anb_rss = (builtins.readFile config.sops.secrets.animebytes_rsskey.path);
in
{
  xdg.configFile."newsraft/feeds".text =
    ''
@ exozyme
https://a.exozy.me/index.xml
https://ersei.net/en/blog.atom
https://www.1a-insec.net/all.atom.xml
https://d-rens.xyz/atom.xml

@ Japanese
$(curl https://apribase.net/program/feed -o ${config.home.homeDirectory}/.config/newsraft/apribase.net.programs) "Apribase (Programs)"
https://qiita.com/tags/linux/feed
https://qiita.com/tags/emacs/feed

@ Technology
https://www.phoronix.com/rss.php
https://lwn.net/headlines/rss
https://thenewstack.io/blog/feed/
https://lobste.rs/rss

@ People
https://unixdigest.com/feed.rss
https://theoryware.net/blog/index.xml
https://andreyor.st/feed.xml
https://wrongthink.link/posts/index.xml

@ YouTube
https://www.youtube.com/feeds/videos.xml?channel_id=UC6nSFpj9HTCZ5t-N3Rm3-HA "Vsauce"
https://www.youtube.com/feeds/videos.xml?channel_id=UCASM0cgfkJxQ1ICmRilfHLw "Patrick Boyle"
https://www.youtube.com/feeds/videos.xml?channel_id=UCcCeJ3pQYFgvfVuMxVRWhoA "もしもしゆうすけ"
https://www.youtube.com/feeds/videos.xml?channel_id=UCrqM0Ym_NbK1fqeQG2VIohg "Tsoding Daily"
https://www.youtube.com/feeds/videos.xml?channel_id=UCe96Qdrn6UGeZ3cB3TxjYKw "Tùng Tùng Soong"
https://www.youtube.com/feeds/videos.xml?channel_id=UC2_krAagEXVPftDXZCDiVZA "Kaname Naito"
https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA "Luke Smith"
https://www.youtube.com/feeds/videos.xml?channel_id=UC9-y-6csu5WGm29I7JiwpnA "Computerphile"
https://www.youtube.com/feeds/videos.xml?channel_id=UC88oKpyXNid09t1m_PZlvfQ "DyingLlama"
https://www.youtube.com/feeds/videos.xml?channel_id=UC4kAu36j1X8qGXys4ZEc77Q "DragonGJY"
https://www.youtube.com/feeds/videos.xml?channel_id=UCRcgy6GzDeccI7dkbbBna3Q "LEMMiNO"
https://www.youtube.com/feeds/videos.xml?channel_id=UCR-DXc1voovS8nhAvccRZhg "Jeff Geerling"
https://www.youtube.com/feeds/videos.xml?channel_id=UCx6SsuNFk6j-iDJJD3pB-qw "Lữ Hành Gia"
https://www.youtube.com/feeds/videos.xml?channel_id=UC25bbxFelbTLAjrLArRbzcg "Eggsterr"

@ Torrent
https://animebytes.tv/feed/rss_torrents_printed/${anb_rss} "AnimeBytes (Printed Media)"
https://animebytes.tv/feed/rss_torrents_games/${anb_rss} "AnimeBytes (Games)"
https://animebytes.tv/feed/rss_torrents_music/${anb_rss} "AnimeBytes (Music)"

@ News
'';
}
