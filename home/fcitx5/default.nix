{ pkgs, ... }: {
  i18n.inputMethod = {
    enabled = "fcitx5";
    fcitx5 = {
      waylandFrontend = true;
      addons = with pkgs; [
        fcitx5-anthy
        fcitx5-bamboo
        fcitx5-gtk
        kdePackages.fcitx5-qt
        fcitx5-fluent # Theme
      ];
    };
  };
}
