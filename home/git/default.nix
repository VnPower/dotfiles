{ ... }:

{

  programs.git = {
    enable = true;

    userEmail = "vnpower@disroot.org";
    userName = "VnPower";

    lfs = {
      enable = true;
      skipSmudge = true;
    };

    extraConfig = {
      # https://jeppesen.io/git-commit-sign-nix-home-manager-ssh/
      # Actually, just read the Git manual
      commit.gpgsign = true;
      user.signingkey = "48E7C060CF12F713";
    };
  };
}
