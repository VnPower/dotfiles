{ pkgs, ... }:

{
  programs.mbsync.enable = true;
  programs.msmtp.enable = true;
  programs.aerc = {
    enable = true;
    extraConfig.general.unsafe-accounts-conf = true;
  };

  accounts.email = {
    accounts.disroot = {
      address = "vnpower@disroot.org";
      aliases = [
        "vnpower@getgoogleoff.me"
        "vnpower@disr.it"
      ];
      realName = "VnPower";
      userName = "VnPower";
      imap = {
        host = "disroot.org";
      };
      smtp = {
        host = "disroot.org";
      };
      gpg = {
        key = "48E7C060CF12F713";
        signByDefault = true;
      };

      mbsync = {
        enable = true;
        create = "maildir";
      };
      msmtp.enable = true;
      aerc = {
        enable = true;
        extraAccounts = {
          source = "maildir://~/Maildir/disroot";
        };
      };
      passwordCommand = "pass vnpower@disroot.org";
    };
    accounts.loang = {
      primary = true;
      address = "vnpower@loang.net";
      realName = "VnPower";
      userName = "vnpower@loang.net";
      imap = {
        host = "loang.net";
        tls.enable = false;
      };
      smtp = {
        host = "loang.net";
        tls.enable = false;
      };
      gpg = {
        key = "48E7C060CF12F713";
        signByDefault = true;
      };

      mbsync = {
        enable = true;
        create = "maildir";
      };
      msmtp.enable = true;
      aerc = {
        enable = true;
        extraAccounts = {
          source = "maildir://~/Maildir/disroot";
        };
      };
      passwordCommand = "pass vnpower@loang.net";
    };
  };
}
