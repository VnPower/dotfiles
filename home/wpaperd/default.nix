{ config, ... }: {
  programs.wpaperd = {
    enable = true;
    settings = {
      path = "${config.home.homeDirectory}/Pictures/wall";
      duration = "20m";
      mode = "fit-border-color";
    };
  };
}
