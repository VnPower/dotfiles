_:
let
  base01 = "1F1F28";
  base00 = "16161D";
  base02 = "223249";
  base03 = "54546D";
  base04 = "727169";
  base05 = "FFFFFF";
  base06 = "C8C093";
  base07 = "717C7C";
  base08 = "C34043";
  base09 = "FFA066";
  base0A = "C0A36E";
  base0B = "76946A";
  base0C = "6A9589";
  base0D = "7E9CD8";
  base0E = "957FB8";
  base0F = "D27E99";
in
{
  qt = {
    enable = true;
    platformTheme.name = "qtct";
    style.name = "kvantum";
    # style.name = "adwaita-dark";
    # style.package = pkgs.adwaita-qt;
  };

  home.sessionVariables = {
    # Scaling factor for QT applications. 1 means no scaling
    QT_AUTO_SCREEN_SCALE_FACTOR = "1";

    # Use Wayland as the default backend, fallback to XCB if Wayland is not available
    QT_QPA_PLATFORM = "qt5ct:qt6ct";

    # Disable QT specific window decorations everywhere
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";

    # Do remain backwards compatible with QT5 if possible.
    DISABLE_QT5_COMPAT = "0";

    # Tell Calibre to use the dark theme, because the
    # default light theme hurts my eyes.
    CALIBRE_USE_DARK_PALETTE = "1";
  };

  xdg.configFile = {
    "Kvantum" = {
      source = ./Kvantum;
      recursive = true;
    };
    "Kvantum/kvantum.kvconfig".text = "[General]\ntheme=Kanagawa";
    "qt5ct/qt5ct.conf".source = ./qt5ct.conf;
    "qt6ct/qt6ct.conf".source = ./qt6ct.conf;
  };
}
