#! /usr/bin/env bash

sed -i Kanagawa.svg -e "s/#353945/#16161d/g"
sed -i Kanagawa.svg -e "s/404552/2A2A37/g"
sed -i Kanagawa.svg -e "s/444a58/363646/g"
sed -i Kanagawa.svg -e "s/2f343f/1F1F28/g"
sed -i Kanagawa.svg -e "s/#383c4a/#1f1f28/g"
# sed -i Kanagawa.svg -e "s/5294e2/7E9CD8/g"
