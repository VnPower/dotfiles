import { App, Astal, Widget } from "astal/gtk3"
import style from "./style.scss"
import MprisPlayers from "./widget/MediaPlayer"

const { TOP, BOTTOM, LEFT, RIGHT } = Astal.WindowAnchor
const { IGNORE } = Astal.Exclusivity
const { EXCLUSIVE } = Astal.Keymode

App.start({
    instanceName: "players",
    css: style,
    main: () => {
        new Widget.Window({}, MprisPlayers())
    }
})
