{ pkgs, ... }:

{
  fonts.fontconfig = {
    enable = true;
    defaultFonts = {
      emoji = [
        "Symbols Nerd Font"
        "Noto Color Emoji"
        "Noto Emoji"
      ];
      monospace = [
        "UDEV Gothic 35NF"
        "IBM Plex Mono"
      ];
      sansSerif = [
        "IBM Plex Sans"
        "PlemolJP35 HS"
        "Noto Sans CJK JP"
      ];
    };
  };


  home.file = {
    # Openbox/Labwc theme
    ".themes/koto/" = {
      source = ../rc/labwc/theme/koto;
      recursive = true;
    };
  };

  xdg.configFile = {
	  "scripts" = {
		  source = ../rc/scripts;
		  recursive = true;
	  };
    "labwc/environment".source = ../rc/labwc/environment;
    "labwc/rc.xml".source = ../rc/labwc/rc.xml;
    "labwc/autostart".source = ../rc/labwc/autostart;
	  "uair/uair.toml".source = ../rc/uair/uair.toml;
  };
}
