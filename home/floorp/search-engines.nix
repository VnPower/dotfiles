{
  # Change Wikipedia alias and hide default engines
  "Wikipedia (en)".metaData.alias = "@wiki";
  "Google".metaData.hidden = true;
  "Amazon.com".metaData.hidden = true;
  "Bing".metaData.hidden = true;

  # Specific search
  "AlpineLinux Wiki" = {
    urls = [ { template = "https://wiki.alpinelinux.org/w/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@alpine" ];
  };

  "ArchLinux Wiki" = {
    urls = [ { template = "https://wiki.archlinux.org/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@arch" ];
  };

  "Gentoo Wiki" = {
    urls = [ { template = "https://wiki.gentoo.org/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@gentoo" ];
  };

  "NixOS Wiki" = {
    urls = [ { template = "https://wiki.nixos.org/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@nixos" ];
  };

  "Nix Packages" = {
    urls = [{
      template = "https://search.nixos.org/packages";
      params = [
        { name = "type"; value = "packages"; }
        { name = "query"; value = "{searchTerms}"; }
      ];
    }];
    # icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
    definedAliases = [ "@nixp" ];
  };

  "GitHub" = {
    urls = [ { template = "https://github.com/search?q={searchTerms}"; } ];
    definedAliases = [ "@github" ];
  };

  "Godocs" = {
    urls = [ { template = "https://godocs.io/?q={searchTerms}"; } ];
    definedAliases = [ "@godoc" ];
  };

  "MDN" = {
    urls = [ { template = "https://developer.mozilla.org/en-US/search?q={searchTerms}"; } ];
    definedAliases = [ "@mdn" ];
  };

  "Stackoverflow" = {
    urls = [ { template = "http://stackoverflow.com/search?q={searchTerms}"; } ];
    definedAliases = [ "@so" ];
  };

  "Youtube" = {
    urls = [ { template = "https://www.youtube.com/results?search_query={searchTerms}"; } ];
    definedAliases = [ "@yt" ];
  };

  # Web search
  # SearX with preferences, feel free to use them
  "SearX-All" = {
    urls = [{
      template = "https://searx.aishiteiru.moe/searxng";
      params = [
        { name = "q"; value = "{searchTerms}"; }
        { name = "preferences"; value = "eJx1WMmO4zgS_Zr2RWiju2swgzn41MBcp4Hpu0CRYSlKXFRcbCu_fiK0WMFU1iGV1iMZDMb6KK0y9CEipFsPHqKyF6t8X1QPN2XpJWhl4Qb-okoOOrjJQoZbH0Jv4XJXD9TBtxFSsA-Itws6WtlOMbzm29-xwMVBHoK5_fXf__19SeoOCVTUw-23Sx7AwS0hS7yQgGJzakmWh2ebVXf7j7IJLibgIT0oer2G2F_WZW3KMylnVBwvGnyG2CqLvXf0e91dmYfyGky7bbsK_VEgzi36NmOm9SuI_o4eMwnVMVi7oes61kuvlppJlAW9yR9CHmFONwN3RQe4lGjbe4hO5Yy-v00Rcp4vBpPqLGkBvkdPpv72z982we1m9V_--PMNNg80EFLbrv9p6N-96ts2BY3KNg4MKgLJGB00ibwytu1i91SjqxwhRo6pYpDGXEmoecjCS3kTUUmVlJ1I38aiL69mUnrkTWizzGPT2DiMMcS2vaNdN5_IKw09eZMIYsArihOjtrUeXyE1TxxxF5ZGuW-HFvlP6N6RPespuSt6hLxJ6KauGs4G-_5QYDHaVWt9NSCkahuKuVsVQaFcridy3R0iUPBsG1BUpEQg-UEj4yv2NA-kiBVrZ5g-vTard4SXGKRAJ4ifLIkiB0GuMwAfFNBvDxlIWWWkJYFSJzJi-oYCj6MWg09yMek309nOQQakMRohg2zIf304KynGThEpxqpzCPwJijc59lqjw6mJptOTtXHhO07smmMWJQ7MlSV-fwml7iYGPsDuWDq-yYPKjqqJnBaBIj3c85N82xiMlLKcvKsv7xH9iErLBfMszNIDfgxUAARCpYECKLhNBFVAQ1Fj5JSlKDaTVTPnQDqUlCMuUMBIU1KxgMiJzaVU2n9QXVT82LYcKAEhruZeAXi9g5OEpCtn9PKOznSHYuj6EoVg9Ersj55-Yijpa2yPP_JTpEibhRxK0qji3LCrEorz8gDkqHyypJf0JbUDpSflNz1d-BiqxHNP11kJ-Gm3eDBzApm8YQK_qXCclUHahOuehCJMQSi41jJMe_WYkA3YqVlMiXBOnzd4zpZjaE-Wr5ZVyTKVjsrRY9Phx1P5LFdFMAbzqe5H7IfcGBWquSFn6nqBgimAiMl1WzcnrP2QQh_KCTifaoVP6b_CT9CUeUfuY9Ilpes0U8vfPayVMTPnjStb9Kz2_nUIFk74qq4OBhp-vFFHpkHdJE2rVJQlmIpizBOTDnGYHMY55JCGMHKo7S7NfCiSoaqTS3Q7Sa7SZ1ZDqIz9xG6u30fsQhjTZ_BHCbXZGUyhRH1GJ9BLXfgJfJyYYWJDabFaPfsR5k-WeAZ7j8pRIx-qzv77t2__eh2WMcWAl1Hz4ZWr8jJ8BxjPyDliNrwK9KXmTWhDPvbw6sHNQYRw6eYe3F7cJoCYSycTfskAWj5yy31CJ4ZmpStr8PtZt1ioukiJliyZqBhI7BleOAZPmdmk2Qc_c2M5LANjrA-3QufMWeBz5lBZ1FWap0fP1UksfVCHWzjXZgp2raM2Xx2wOGdF3aMy-Kg8T5XxGhdjXsDX9FN5r1LDmYYPSdK4QK1cT1KzSJknCWZ84UPGY0e8USs3Ha2CV29dXRI235-ttKCVORfkZDau4Rz0ssfrAe5j4AK9G4FYFFO2ueZRD6TqQceQ1CZwK22G0gkTG5UrjQ8uUzGoPLtAfceLqLhb1KPssDQdZfPsuTbvzZmohFXd8XJowTWvg9gfHKO3lVM3InEy44ZXhtywkyk3_ItaOmz7LZt_RyqfxxFHOqFKsseDc3OzF_C1RtWNap1REsSfjVFPzj8bY8lktfPwx6_vpi_5BhORcKdlntIppYqm-P5RedaGLmXY0mOhYHRfMcH_RNn38KDSQO3iixnmzWjwtVwqDt_7WSlxEVHEPGGndBzWisgph16CXNX5fYxYAoWX2jh-PU7VBCATmd4Z9WS4Yh6TJrqhci_eRpEvyZBkKkycqiI8lvfrovxxhilQiiei5oYp5x4Tq2aNIdaBfg3Ko6ocG5TO8Q5CqXlCSXrquF2hU4iv8CmaoyKe2nR0rUnytpSo3aidvwnaUqhW1e4hAqHHQLXlbsPzuI6WrvhcdgJSiPyW9Hbbm3R86gRv-AseZdHQDSSuIb1HQ_GJLgVpkFdDZhu13DmUT53wjbxvhwot1SUOEjHtga76AMBVrpJMAC-pGDRP4uSjcLueziEHT76Qg5--L8ih5fCCe6DOH8HX1wRquo7ua837EmFqVhONx1FcGkPIfBNkD31KzaeydqDe6OUpco5X9J-65_m0K3w65wrXrl9I3VDyUbrrzxH_EB8jprG_9mGn_sfnockWatDp9m7YO8CcgfpVy5-XIhnmsSjN1WibciHGS9Xg9n8Vdnh1"; }
      ];
    }];

    definedAliases = [ "@search" ];
  };
  "SearX-JP" = {
    urls = [{
      template = "https://searx.aishiteiru.moe/searxng";
      params = [
        { name = "q"; value = "{searchTerms}"; }
        { name = "preferences"; value = "eJx1WMuO6zgO_ZrOxrjB9PQAjVlkNcAsejMNTO8NWmJsXcuiryQncX19k37EVLnuolIVUs9D8vCoDGRsKTpMtxYDRvAXD6GdoMXbd_j2x58XTwY83jBcYMpkaBg9Zry1RK3Hyx0ezlCoIybyD4y3ixt4bj1Ges23v-KElwFzR_b25__-_9clwR0TQjTd7R-X3OGAt-RkxQsvMPmcal4r4LPO0Nz-Cz7hxZI7Vifgr1eK7WWdVqc88-EsxP5iMGSMNXjXhoH_XncH-4Bg0NbbtuuiPyaMc-1CnV3m-avRhbsLLvOiJpL3m3WdJ-cyK1YzL-XRbOt3lHuc083iHfgClyn6-k5xgJxdaG9jxJzni3UJGs-nwNC6wGD_u4W2rhMZB74a0Dr45Z__4cs1WCVGua_rBcdUWquHs0h1vfz67IPJOvYNU3JGXB5fEGx0UNdbaBfryPtX3oXpVY1getmEN8viG_tqcDFSrOu78-vmI6Nc8adsElE5AnDcLexzU6-3aZx38qOO2jAc5ZDcTKbHvK3QjE3hzta17bHfgtHVGHO1qFY1niZ79xARnJ5uRkb-jhE59tsGHNSU2MiwGyf21fa0D8cJp-bOOH76Wq3BUEERI-cpm-RTVuLAO9TzLOIH5-M7IBZThux4CnHmR7HYtuK8kaRzFJKezOeb-W5b8mkHn9hZtQZjKD8tnQ-pfCtgCjjlK-6h7E8E2eTYa02GAUYezp9ymoG-u1FCc4zivMe5QOLXlzrU3UaSC-yB5evb3EEemAz0sIic2HTPT45tZV3kipPaW2N5jy70DoyeMM8KlhbdR8f1qyxc2ZxANGxLMIFZzhqrhyycVo0eZkn5dBxSewbihNFQcq1jlDoWJtT4d9BEkI9ty47rDeMK92rA1zs5eZF0lQJevrvBNsfB3NBOUS3sAqj9XeA_HU3pa9uef1yREeJcSVySU5cTB-YIIXk-hA4cUzeYEcJ2qIE-uqLKhufQeG0I4w4v2TmhrlQaMWxHOC4mRt5EOE2bIo6kDrjylEs7VYxO0GpgVkMinmvlbTyXxuHaK-OraUVljFPD3PPYzvDjCSHrWRGtdfnE6dG1Xa4sUDGWcuYORZw5hCoB122HObkyDtw-zJTSdZy5le7RMGDtLAk9TEwg8xubbx15PNnXpQ1ZrOTjbR34Gs5UyfAsiJobma1iHqWZq5Nk6mfKlDrqJS12-LPAymvAVAxW1g3EXOT1DB0VwDxdM5ffe9cQ9emz8cdEJURiTDRFc7aOaJaC_Yn5uLGYWWWkBbVy9IPmT0g8yd8jDNxQu6LD_vrbb7-_DmTsZDHoCH8EGIoaou-I_dlyztnNXiTlQkaj85SPPQI8hLVVuk3N3OKws86IGPPU6OJcspWn99ILn9go1wymQEO-n88WJ2YCvaJnJBMXrrY96eV6ClxFVZoDhVkY_0AG-1hebjWd9lrNp46WmMJMUZLp0QqTqKkPbj2L9tmgkNAO3H-LC07D4BVHMWU9isgzi13jAuYFQynrIARIlVSae2ixJGSyai7ZcddMkStPC734cg-djw3rNwPDeHC4zN7arVZSoT2jtFgLOBfLCTbhW0l63XxNh_eehEx3EFjeiJaaS4HzcMwefA2tOUh6XNVNjYLYQi5OfIiMQtrkeSDuEUFlxd070-vWx8Od7mqt8OjeNbnHe2iOL8cphPMajO3R_FtfBHXr8CcYN3sB5GY7QbnZv-DSbttv2fy7Y_o8rtjzDSHpfozDMFc7ga8cVTaVdcSUMP7Mx_0z_8wnKzNqZ_fHt3eD1tpARAPdeVrgckoaHI5V-ygi66lJGbfyWLQRvxsshZ8c9u3uIHXcLr4YYd_qw70WtX_EPswA6oUALAlx11qS1sCqUVIvYS54fvdxR-f0gk18l35mE8TMKneXuqMVxjwGjfzyk168eZ08PjHpUhilVFV6LN-vy-GPO4zEJZ5YM1vRgntO8MkGmau2m0enpUeZkavplLyr-ZSnEVgaVg2_JJJ-oCRuJLCrqINWaWIWKoFnaWB6Yta4e3oeL8CpmUKedmkxjRin9A4Iv-CdZQkf19TbozaFxKo6dfptJaqgvN9M06eO9ba8n1fgPPOHBFMNe7iheDALGxUrs0GmFKpUBkmRcFpcT5hq5wlZ7fz0Hteu5fJKIziTPyiU0pub48APnuotzG2pPqINrlevLqIsTynB-1MJPcH7jntY0LfIOV5d-NTlzrddzad7ruayWS_iq5vyQbHle_5f6jU_9u21pV1OH_8eGf3EjTTdJAtf1-3blaBe_wv0jCyMf1nF7OFO6O-1C3c6eTjdau5lpn836q83MODNxBhTPC0hxLTNOvuYZFnNbaKk2DguT71a_hsUOY4PPK8s5F4zqbKPqePCyppZ5_Y3lOFjsw=="; }
      ];
    }];

    definedAliases = [ "@searchjp" ];
  };
}
