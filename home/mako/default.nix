{config, pkgs, ...}: {
  services.mako = {
    enable = true;

    anchor = "top-right";
    backgroundColor = "#1f1f28fe";
    borderColor = "#363646dd";
    borderRadius = 5;
    borderSize = 1;
    defaultTimeout = 10000;
    width = 300;
    height = 200;
    maxIconSize = 96;
    font = "IBM Plex Mono 10";
    extraConfig = ''
on-notify=exec ${pkgs.pipewire}/bin/pw-play ${config.home.homeDirectory}/Audio/sfx/pope2.mp3
history=1
max-history=99
'';
  };
}
