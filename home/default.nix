{ config, pkgs, pkgs-stable, lib, inputs, ... }:

let
  inherit (config) xdg;
in
{
  imports = [
    ./emacs
    ./direnv
    ./fish
    ./floorp
    ./foot
    ./git
    ./go
    ./mpv
    ./newsraft
    ./nvim
    ./pass
    ./tmux
    ./wpaperd
    ./xdg
    ./yazi

    ./fd
    ./fastfetch
    # ./senpai
    ./hexchat
    ./hyprlock
    ./fuzzel
    ./waybar
    ./mako
    ./qt
    ./gtk
    ./gtklock
    ./fcitx5

	  ./desktop.nix
	  ./games.nix
    ./email.nix
  ] ++ lib.optional (builtins.pathExists ./cursor.nix) ./cursor.nix;

  home = {
    stateVersion = "24.11"; # Please read the comment before changing.
    packages = with pkgs; [
      # TODO Lock screen
      # TODO Proxy setup

      # System
      labwc

      # Programming language
      go
      python3
      ruby
      clojure
      zig
      lua
      lua54Packages.fennel
      nodejs_23

      # Programming packages
      luarocks

      # Compilers
      gcc
      pkg-config
      dart-sass
      gnumake

      # Programming tools
      glade
      hugo
      zola
      air
      aspell
      ispell

      # LSP
      gopls # Go
      lua-language-server
      zls # Zig
      clojure-lsp
      ccls # C/C++
      rubyPackages.solargraph
      fennel-ls
      nil # Nix
      lua53Packages.lua-lsp
      yaml-language-server
      vscode-langservers-extracted # HTML/CSS
      typescript-language-server

      # Internet

      ## Communication
      senpai
      gajim
      abaddon
      materialgram
      kdePackages.neochat
      claws-mail

      ## Downloads
      qbittorrent
      nicotine-plus

      ## Tools
      syncthing
      virtualgl
      # birdtray

      # Design
      gimp
      krita
      inkscape

      # Images
      imgbrd-grabber
      nsxiv
      digikam
      imagemagick
      exiftool
      mediainfo
      oxipng
      jpegoptim
      xcur2png

      # Music
      pkgs-stable.quodlibet-full
      picard
      jamesdsp
      pavucontrol
      flac2all
      navidrome

      # Documents
      libreoffice
      pandoc
      texliveMedium
      mate.atril
      udiskie
      (calibre.override {
        unrarSupport = true;
      })

      # Video
      ffmpeg-full
      handbrake
      memento
      wf-recorder
      yt-dlp
      streamlink

      # Desktop
      sway-contrib.grimshot
      swaybg
      swww
      mpvpaper
      waypaper
      gtklock
      gtklock-userinfo-module
      chayang
      swayidle
      wlopm
      goldendict-ng
      xorg.xeyes

      # Self-improvement
      # klavaro
      amphetype
      obsidian
      anki-bin
      uair

      # TUI utilities
      foot
      htop
      btop
      fzf
      tmux
      newsraft

      # CLI tools
      cliphist
      wl-clipboard
      playerctl
      distrobox
      libnotify
      appimage-run
      smartmontools
      inputs.nix-alien.packages.${system}.nix-alien

      ## Internet
      rsync
      rclone
      neocities
      megatools

      ## Terminal
      tree
      fd
      ripgrep
      jq
      file
      semgrep
      gavin-bc

      # Shell
      grc

      ## Cryptography
      openssl
      age
      sops
      keepassxc

      ## Archives
      zip
      unzip
      p7zip
      unrar
      ouch

      # Settings
      kdePackages.breeze-gtk
      kdePackages.qt6ct
      libsForQt5.qt5ct
      kdePackages.qtwayland
      kdePackages.qtstyleplugin-kvantum

      # Wine
      wine
      wineWowPackages.fonts
      # wineWowPackages.waylandFull
      winetricks
      lutris

      # Games
      crawlTiles
      (retroarch.withCores (cores: with cores; [
          desmume
          dosbox
      ]))
      # factorio-demo
      # shattered-pixel-dungeon
      # unciv
      # sauerbraten
      # xonotic
      # openttd
    ];

    sessionPath = [
      "$HOME/.local/bin"
      "$HOME/.config/scripts"
    ];

    sessionVariables = {
      EDITOR = "emacs";

      GOPATH = "${xdg.dataHome}/go";
    };
  };

  sops = {
    age.keyFile = "/home/vnpower/.config/sops/age/keys.txt"; # must have no password!

    defaultSopsFile = ./secrets.yaml;
    defaultSymlinkPath = "/run/user/1000/secrets";
    defaultSecretsMountPoint = "/run/user/1000/secrets.d";

    secrets.passphrase = {
      # sopsFile = ./secrets.yml.enc; # optionally define per-secret files
      path = "${config.sops.defaultSymlinkPath}/passphrase";
    };
    secrets.animebytes_rsskey.path = "${config.sops.defaultSymlinkPath}/animebytes_rsskey";
    secrets.soju_key.path = "${config.sops.defaultSymlinkPath}/soju_key";
  };

  # The home.packages option allows you to install Nix packages into your
  # environment.
  # Let Home Manager install and manage itself.
  programs.home-manager = {
    enable = true;
    # sharedModules = with inputs; [
    #     inputs.nur.modules.homeManager.default
    #     sops-nix.homeManagerModules.sops
    # ];
  };
}
