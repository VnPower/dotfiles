{ ... }:

{
  programs.go.telemetry = {
    mode = "off";
    date = "2025-01-01";
  };
}
