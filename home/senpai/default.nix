{ config, ... }:
{
  programs.senpai = let
    soju_key = (builtins.readFile config.sops.secrets.soju_key.path);
  in {
    enable = true;
    config = {
      address = "loang.net";
      nickname = "vnpower";
      password = "${soju_key}";
    };
  };
}
