{ ... }: {
  environment.etc = {
    openvpn.source = ./openvpn;
  };
  services.openvpn.servers = {
    testVPN = { config = '' config /etc/openvpn/testVPN.conf '';};
  };
}
