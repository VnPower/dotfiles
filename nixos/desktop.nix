{ pkgs, config, lib, ... }:

{
  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  security = {
    pam.services.greetd.enableGnomeKeyring = true;
    pam.services.gtklock = {};
    rtkit.enable = true;
  };

  services = {
    fstrim.enable = true;
    gnome.gnome-keyring.enable = true;
    openssh.enable = true;
    gvfs.enable = true;
    tumbler.enable = true;
    # libinput.enable = true; # Touchpad support

    udev.packages = with pkgs; [ gnome-settings-daemon ];

    dbus.packages = with pkgs; [
      gcr
      gnome-settings-daemon
    ];

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    greetd = {
      enable = true;
      settings = {
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd labwc";
          user = "greeter";
        };
      };
    };

    suwayomi-server = {
      enable = false;

      dataDir = "/var/lib/suwayomi"; # Default is "/var/lib/suwayomi-server"
      openFirewall = true;

      settings = {
        server.port = 4567;
        server.enableSystemTray = true;
        server.autoDownloadNewChapters = false;
        server.extensionRepos = [
          "https://raw.githubusercontent.com/keiyoushi/extensions/refs/heads/repo/index.min.json"
        ];
      };
    };

    tor = {
      enable = true;
      relay.enable = false;
      client = {
        enable = true;
        socksListenAddress = {
          addr = "127.0.0.1";
          port = 9050;
          IsolateDestAddr = true;
        };
      };
    };
  };

  systemd.services.greetd.serviceConfig = {
    Type = "idle";
    StandardInput = "tty";
    StandardOutput = "tty";

    # without this errors will spam on screen
    StandardError = "journal";

    # without these bootlogs will spam on screen
    TTYReset = true;
    TTYVHangup = true;
    TTYVTDisallocate = true;
  };

  programs = {
    dconf.enable = true;
    seahorse.enable = true; # enable the graphical frontend
    cdemu.enable = true;
    xfconf.enable = true;
    gnupg.agent.enable = true;
    fish.enable = true;
    gamemode.enable = true;
    nm-applet.enable = true;

    nix-ld = {
      enable = true;
      libraries = with pkgs; [
        glibc
      ];
    };

    ssh = {
      startAgent = true;
      enableAskPassword = true;
    };

    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        thunar-volman
        thunar-archive-plugin
      ];
    };
  };

  hardware = {
    enableRedistributableFirmware = true;
	  graphics = {
		  enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [
        intel-ocl
        intel-media-driver
        intel-vaapi-driver
        vaapiIntel
        vaapiVdpau
        libvdpau-va-gl
        mesa.drivers
      ];
	  };
  };
}
