{ pkgs, inputs, ... }:

let
  StateDirectory = "dnscrypt-proxy";
  blocklist_base = builtins.readFile inputs.oisd;
  extraBlocklist = '''';
  blocklist_txt = pkgs.writeText "blocklist.txt" ''
    ${extraBlocklist}
    ${blocklist_base}
  '';
in
{
  networking = {
    networkmanager.enable = true;  # Easiest to use and most distros use this by default.

    nameservers = [ "127.0.0.1" "::1" ];
    networkmanager.dns = "none";
    useDHCP = false;
    dhcpcd.enable = false;
  };

  services.dnscrypt-proxy2 = {
    enable = true;

    settings = {
      sources.public-resolvers = {
        urls = [
          "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
        ];

        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
        cache_file = "/var/lib/${StateDirectory}/public-resolvers.md";
      };

      blocked_names.blocked_names_file = blocklist_txt;

      # I don't have IPv6
      ipv6_servers = false;

      require_dnssec = true;
      require_nolog = true;
      require_nofilter = true;

      server_names = [
        # cryptostorm.is
        "cs-tokyo"
        "cs-singapore"
        "cs-sk" # South Korea

        "dnscry.pt-singapore-ipv4"
        "dnscry.pt-singapore02-ipv4"
        "dnscry.pt-seoul-ipv4"
        "dnscry.pt-taipeh-ipv4"
        "dnscry.pt-hongkong-ipv4"
        "dnscry.pt-hongkong02-ipv4"

        "jp.tiar.app"
        "jp.tiar.app-doh"
      ];
    };
  };

  systemd.services.dnscrypt-proxy2.serviceConfig.StateDirectory = StateDirectory;

  programs.nm-applet.enable = true;

}
