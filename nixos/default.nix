{ pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./overlays.nix
      ./desktop.nix
      ./network.nix
      ./vpn.nix
      ./hardware-configuration.nix
      ./containers
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  # boot.initrd.kernelModules = [ "i915" ];
  boot.kernelParams = [
    "i915.enable_rc6=7"
    # "acpi=off"
    # "processor.max_cstate=0"
    # "intel_idle.max_cstate=0"
    # "idle=nomwait"
    # "pci=noacpi"
    "libata.force=noncq"
    "splash"
  ];


  # Set your time zone.
  time.timeZone = "Asia/Bangkok";

  environment.systemPackages = with pkgs; [
    neovim
    wget
	  git
	  curl
	  unar
    libsecret
    podman-compose
    podman-tui

    gnomeExtensions.appindicator
  ];

  fonts.fontDir.enable = true;
  fonts.packages = with pkgs; [
	  noto-fonts
	  noto-fonts-cjk-sans
	  noto-fonts-emoji
	  fira-code
	  fira-code-symbols
	  plemoljp-nf
	  plemoljp-hs
	  ibm-plex
	  udev-gothic-nf
    nerd-fonts.symbols-only
  ];

  i18n = {
    supportedLocales = [
      "en_US.UTF-8/UTF-8"
      "ja_JP.UTF-8/UTF-8"
    ];
	  defaultLocale = "en_US.UTF-8";
  };

  console = {
    earlySetup = true;
    font = "Lat2-Terminus16";
    useXkbConfig = true; # use xkb.options in tty.
  };

  nix = {
    package = pkgs.nixVersions.latest;

    # Keep the desktop responsive when switching to a new generation
    # Ref: https://github.com/nix-community/srvos/blob/main/nixos/common/nix.nix
    daemonCPUSchedPolicy = "batch";
    daemonIOSchedClass = "idle";
    daemonIOSchedPriority = 7;

    settings = {
		  auto-optimise-store = true;
      auto-allocate-uids = true;
      sandbox = true;
      use-xdg-base-directories = true;
      http-connections = 0;
      max-jobs = "auto";
      warn-dirty = false;
      download-buffer-size = 50000000;
		  experimental-features = ["nix-command" "flakes" "auto-allocate-uids"];
      log-lines = 25; # 10 is horribly low

      # build from source if substitutes fail
      fallback = true;

      # for nix-direnv
      keep-outputs = true;
      keep-derivations = true;

      # Fallback quickly if substituters are not available.
      connect-timeout = 5;

      # Avoid copying unnecessary stuff over SSH
      builders-use-substitutes = true;

      substituters = [
        "https://nix-community.cachix.org"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
	  };

	  gc = {
		  automatic = true;
		  dates = "weekly";
		  options = "--delete-older-than 3d";
      persistent = true;
	  };
  };

  systemd.services.nix-gc.serviceConfig = {
    CPUSchedulingPolicy = "batch";
    IOSchedulingClass = "idle";
    IOSchedulingPriority = 7;
  };

  # Make builds to be more likely killed than important services.
  # 100 is the default for user slices and 500 is systemd-coredumpd@
  # We rather want a build to be killed than our precious user sessions as builds can be easily restarted.
  systemd.services.nix-daemon.serviceConfig.OOMScoreAdjust = 250;

  # Configure keymap in X11
  services = {
    xserver = {
      xkb = {
        layout = "us";
        variant = "colemak";
      };
      videoDrivers = [ "intel" ];
    };
  };

  users = {
    users.vnpower = {
      shell = pkgs.fish;
      isNormalUser = true;
      extraGroups = [ "wheel" "networkmanager" "cdrom" "plugdev" "gamemode" ];
    };
  };

  nixpkgs = {
    config = {
      packageOverrides = pkgs: {
        intel-vaapi-driver = pkgs.intel-vaapi-driver.override {enableHybridCodec = true;};
      };

      permittedInsecurePackages = [
        "olm-3.2.16"
      ];

      allowUnfree = true;
    };
  };

  environment.sessionVariables = {
    LIBVA_DRIVER_NAME = "i965";
    VDPAU_DRIVER = "va_gl";
    XDG_RUNTIME_DIR = "/run/user/$UID";
    NIXOS_OZONE_WL = "1";
  };

  system = {
    stateVersion = "24.11";
  };
}
