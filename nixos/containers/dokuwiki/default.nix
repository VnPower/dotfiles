{ lib, ... }:
let
  name = "dokuwiki";
in
{
  system.activationScripts."make-${name}-dir" =
    lib.stringAfter ["var"]
      ''mkdir -v -p /etc/oci.d/${name}/config && chown -R 1000:1000 /etc/oci.d/${name}/config'';

  virtualisation.oci-containers.containers = {
    dokuwiki = {
      image = "lscr.io/linuxserver/dokuwiki:latest";
      ports = ["127.0.0.1:3002:80"];
      volumes = [
        "/etc/oci.d/${name}/config:/config"
      ];
      autoStart = false;
    };
  };
}
