{ lib, ... }:
let
  name = "tachidesk";
in
{
  system.activationScripts."make-${name}-dir" =
    lib.stringAfter ["var"]
      ''mkdir -v -p /etc/oci.d/${name}/downloads && \
      mkdir -v -p /etc/oci.d/${name}/files && \

      chown -R 1000:1000 /etc/oci.d/${name}/downloads && \
      chown -R 1000:1000 /etc/oci.d/${name}/files
'';

  virtualisation.oci-containers.containers = {
    tachidesk = {
      image = "ghcr.io/suwayomi/tachidesk:latest";
      ports = ["127.0.0.1:3004:4567"];
      volumes = [
        "/etc/oci.d/${name}/downloads:/home/suwayomi/.local/share/Tachidesk/downloads"
        "/etc/oci.d/${name}/files:/home/suwayomi/.local/share/Tachidesk/"
      ];
      autoStart = false;
    };
  };
}
