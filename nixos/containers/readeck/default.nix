{ lib, ... }:
let
  name = "readeck";
in
{
  system.activationScripts."make-${name}-dir" =
    lib.stringAfter ["var"]
      ''mkdir -v -p /etc/oci.d/${name}/ && chown -R 1000:1000 /etc/oci.d/${name}/'';

  virtualisation.oci-containers.containers = {
    readeck = {
      image = "codeberg.org/readeck/readeck:latest";
      ports = ["127.0.0.1:3001:8000"];
      volumes = [
        "/etc/oci.d/${name}/:/readeck"
      ];
      autoStart = false;
    };
  };
}
