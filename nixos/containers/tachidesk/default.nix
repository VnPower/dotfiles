{ lib, ... }:
let
  name = "lanraragi";
in
{
  system.activationScripts."make-${name}-dir" =
    lib.stringAfter ["var"]
      ''mkdir -v -p /etc/oci.d/${name}/content && \
      mkdir -v -p /etc/oci.d/${name}/thumb && \
      mkdir -v -p /etc/oci.d/${name}/database && \

      chown -R 1000:1000 /etc/oci.d/${name}/content && \
      chown -R 1000:1000 /etc/oci.d/${name}/thumb && \
      chown -R 1000:1000 /etc/oci.d/${name}/database
'';

  virtualisation.oci-containers.containers = {
    lanraragi = {
      image = "docker.io/difegue/lanraragi:latest";
      ports = ["127.0.0.1:3003:3000"];
      volumes = [
        "/etc/oci.d/${name}/content:/home/koyomi/lanraragi/content"
        "/etc/oci.d/${name}/thumb:/home/koyomi/lanraragi/thumb"
        "/etc/oci.d/${name}/database:/home/koyomi/lanraragi/database"
      ];
      autoStart = false;
    };
  };
}
