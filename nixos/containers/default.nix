{ ... }:
{
  imports = [
    ./readeck
    ./lanraragi
    ./dokuwiki
    ./tachidesk
  ];

  virtualisation = {
    containers.enable = true;
    podman = {
      enable = true;
      dockerCompat = true;
      autoPrune.enable = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };
}
