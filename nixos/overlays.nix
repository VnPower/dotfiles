{ inputs, ... }:

{
  nixpkgs.overlays = [
    (final: prev: {
      labwc = prev.labwc.overrideAttrs (old: {
        name = "labwc";
        src = prev.fetchFromGitHub {
          owner = "labwc";
          repo = "labwc";
          rev = "577c24306fe630b8b4ec7b3cefed86511c550cdd";
          hash = "sha256-X9P7lROvSOMPzzr0K9J9bYmOwakNDUeLTmL72NCP0Jo=";
        };
      });
    })
 ];
}
